//
//  AdditionalUtility.swift
//  Tokopedia
//
//  Created by Vanilla Latte on 08/08/18.
//

import Foundation
import Alamofire
import Kingfisher
public class RequestAPI {
    func GET_rest_API(url: String, completion:@escaping([String:Any]) -> ()) {
        request(url).validate().responseJSON() { response in
            switch response.result {
            case .success(let value):
                let jsonObject = try? JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String:Any]
                completion(jsonObject!)
            case .failure(let err):
                let failMsg : [String:Any] = [
                    "status" : "400",
                    "msg" : err.localizedDescription
                ]
                completion(failMsg)
            }
        }
    }
    func POST_rest_API() {
        
    }
}

public class filterModel {
    var wholesale: Bool?
    var pmin : String?
    var pmax : String?
    var goldType: String?
    var officialType : Bool?
    var start : String?
    var rows : String?
}

public class CommonDefinition {
    var mainURL : String = "https://ace.tokopedia.com/search/v2.5/"
    var product : String = "product?"
    var catalog : String = "catalog?"
    var shop : String = "shop?"
    var defaultPMin: String = "10000"
    var defaultPMax: String = "100000"
}

extension UIImageView {
    func imageToLoad(url: String){
        let uri = URL(string: url)
        self.kf.setImage(with: uri)
    }
}

protocol fromSearchToPage {
    func backToSearch(params: filterModel)
}

protocol shopTypeProtocol {
    func applyDone(params: filterModel)
}
