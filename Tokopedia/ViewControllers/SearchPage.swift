//
//  SearchPage.swift
//  Tokopedia
//
//  Created by Vanilla Latte on 08/08/18.
//

import UIKit
import CCBottomRefreshControl
import MaterialComponents

class SearchPage: UIViewController {

    //Declare ToolBox
    @IBOutlet weak var itemsCollection: UICollectionView!
    
    @IBOutlet weak var segmentMenu: UISegmentedControl!
    
    @IBOutlet weak var toolAction: UICollectionView!
    
    
    // ToolBox Function
    
    @IBAction func didSegmentClick(_ sender: UISegmentedControl) {
        products.removeAll()
        self.loadAPI(pmin: filtering.pmin!, pmax: filtering.pmax!, wholesale: filtering.wholesale!, official: filtering.officialType!, fshop: filtering.goldType!, start: "0", rows: "10", index: sender.selectedSegmentIndex)
        self.setupTools(index: sender.selectedSegmentIndex)
    }
    
    //Variable
    let bottomRefresh = UIRefreshControl()
    var searchName = String()
    var products = [[String:Any]]()
    var tools = [[String:Any]]()
    let Req = RequestAPI()
    let def = CommonDefinition()
    var filtering = filterModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomRefresh.triggerVerticalOffset = 50
        bottomRefresh.addTarget(self, action: #selector(self.refreshProduct), for: .valueChanged)
        itemsCollection.bottomRefreshControl = bottomRefresh
        self.navigationItem.title = "Search of " + searchName
        if let flow = itemsCollection.collectionViewLayout as? UICollectionViewFlowLayout {
            let horizontal = flow.scrollDirection == .vertical ? flow.minimumInteritemSpacing : flow.minimumLineSpacing
            let cellW = (view.frame.width - max(0, 2 - 1)*horizontal)/2
            flow.itemSize = CGSize(width: cellW, height: 258)
        }
        self.setDefaultFilter()        
        self.segmentMenu.selectedSegmentIndex = 0
        self.loadAPI(pmin: filtering.pmin!, pmax: filtering.pmax!, wholesale: filtering.wholesale!, official: filtering.officialType!, fshop: filtering.goldType!, start: "0", rows: filtering.rows!, index: 0)
        self.setupTools(index: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func refreshProduct(){
        let count: Int = products.count
        loadAPI(pmin: filtering.pmin!, pmax: filtering.pmax!, wholesale: filtering.wholesale!, official: filtering.officialType!, fshop: filtering.goldType!, start: String(count + 10), rows: filtering.rows!, index: segmentMenu.selectedSegmentIndex)
    }
    
    //Declare Function
    func setDefaultFilter(){
        filtering.wholesale = true
        filtering.officialType = true
        filtering.pmax = "10000000"
        filtering.pmin = "100"
        filtering.rows = "10"
        filtering.start = "0"
        filtering.goldType = "2"
    }
    
    func loadAPI(pmin: String, pmax: String, wholesale: Bool, official: Bool, fshop: String, start: String, rows: String, index: Int){
        var decision : String = ""
        switch(index){
        case 0:
            decision = def.product
        case 1:
            decision = def.catalog
        case 2:
            decision = def.shop
        default:
            break
        }
        let makeURL = def.mainURL + decision + "q=\(searchName)&pmin=\(pmin)&pmax=\(pmax)&wholesale=\(wholesale)&official=\(official)&fshop=\(fshop)&start=\(start)&rows=\(rows)"
        Req.GET_rest_API(url: makeURL) { (result) in
            let status = result["status"] as! [String:Any]
            let header = result["header"] as! [String:Any]
            let data = result["data"] as! [[String:Any]]
            if(status["error_code"] as! Int == 0){
                self.products += data
                self.itemsCollection.reloadData()
                self.setupTools(index: index)
                self.itemsCollection.bottomRefreshControl?.endRefreshing()
            }
        }
    }
    
    @objc func setupTools(index: Int){
        switch(index){
        case 0:
            tools = [[
                    "Text" : "Sort",
                    "Image" : "sort"
                ],
                [
                    "Text" : "Filter",
                    "Image" : "filter"
                ],
                [
                    "Text" : "Grid",
                    "Image" : "grid"
                ],
                [
                    "Text" : "Share",
                    "Image" : "share"
                ]
            ]
            if let flow = toolAction.collectionViewLayout as? UICollectionViewFlowLayout {
                let horizontal = flow.scrollDirection == .vertical ? flow.minimumInteritemSpacing : flow.minimumLineSpacing
                let cellW = (view.frame.width - max(0, 4 - 1)*horizontal)/4
                flow.itemSize = CGSize(width: cellW, height: 58)
            }
        case 1:
            tools = [[
                        "Text" : "Sort",
                        "Image" : "sort"
                ],
                     [
                        "Text" : "Filter",
                        "Image" : "filter"
                ],
                     [
                        "Text" : "Grid",
                        "Image" : "grid"
                ],
                     [
                        "Text" : "Share",
                        "Image" : "share"
                ]
            ]
            if let flow = toolAction.collectionViewLayout as? UICollectionViewFlowLayout {
                let horizontal = flow.scrollDirection == .vertical ? flow.minimumInteritemSpacing : flow.minimumLineSpacing
                let cellW = (view.frame.width - max(0, 4 - 1)*horizontal)/4
                flow.itemSize = CGSize(width: cellW, height: 58)
            }
        case 2:
            tools = [
                [
                        "Text" : "Filter",
                        "Image" : "filter"
                ],
                     [
                        "Text" : "Grid",
                        "Image" : "grid"
                ]
            ]
            if let flow = toolAction.collectionViewLayout as? UICollectionViewFlowLayout {
                let horizontal = flow.scrollDirection == .vertical ? flow.minimumInteritemSpacing : flow.minimumLineSpacing
                let cellW = (view.frame.width - max(0, 2 - 1)*horizontal)/2
                flow.itemSize = CGSize(width: cellW, height: 58)
            }
        default:
            break
        }
        self.toolAction.reloadData()
    }
    func toolsSelected(index: Int){
        let msg = MDCSnackbarMessage()
        switch(index){
        case 0:
            msg.text = "Sorting coming soon~"
            msg.duration = 2
            MDCSnackbarManager.show(msg)
        case 1:
            self.performSegue(withIdentifier: "toFilterPage", sender: self)
        case 2:
            msg.text = "Grid View coming soon~"
            msg.duration = 2
            MDCSnackbarManager.show(msg)
        case 3:
            share()
        default:
            break
        }

    }
    
    func share(){
        let first = "Find \(searchName) at Tokopedia. Click https://www.tokopedia.com"
        let second = UIImage(named: "tokopedia-logo")
        let act: UIActivityViewController = UIActivityViewController(activityItems: [first,second], applicationActivities: nil)
        act.popoverPresentationController?.sourceView = self.view
        act.popoverPresentationController?.sourceRect = self.view.frame
        if #available(iOS 11.0, *) {
            act.excludedActivityTypes = [
                UIActivityType.saveToCameraRoll,
                UIActivityType.markupAsPDF
            ]
        } else {
            act.excludedActivityTypes = [
                UIActivityType.saveToCameraRoll
            ]
        }
        self.present(act, animated: true, completion: nil)
    }
    
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toFilterPage" {
            let vc : FilterPage = segue.destination as! FilterPage
            vc.params = filtering
            vc.backProtocol = self
        }
    }
 

}

extension SearchPage : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == itemsCollection {
            return products.count ?? 0
        }
        else if collectionView == toolAction {
            return tools.count ?? 0
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == itemsCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemCell", for: indexPath) as! ItemCell
            let n = products[indexPath.row]
            cell.pName.text = n["name"] as? String
            if(segmentMenu.selectedSegmentIndex == 0){
                cell.pPrice.text = n["price"] as? String
            }
            else if (segmentMenu.selectedSegmentIndex  == 1){
                cell.pPrice.text = n["price_min"] as? String
            }
            else if (segmentMenu.selectedSegmentIndex == 2){
                cell.pPrice.text = n["city"] as? String
            }
            cell.pImg.imageToLoad(url: n["image_uri"] as! String)
            return cell
        }
        else if collectionView == toolAction {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "toolCell", for: indexPath) as! ToolsCell
            let n = tools[indexPath.row]
            cell.toolsText.text = n["Text"] as? String
            cell.toolsImg.image = UIImage(named: n["Image"] as! String)
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == toolAction {
            toolsSelected(index: indexPath.row)
        }
    }
}

extension SearchPage : fromSearchToPage {
    func backToSearch(params: filterModel) {
        filtering = params
        print(params)
        products.removeAll()
        loadAPI(pmin: params.pmin!, pmax: params.pmax!, wholesale: params.wholesale!, official: params.officialType!, fshop: params.goldType!, start: "0", rows: "10", index: segmentMenu.selectedSegmentIndex)
    }
}

class ItemCell : UICollectionViewCell {
    @IBOutlet weak var pImg: UIImageView!
    @IBOutlet weak var pName: UILabel!
    @IBOutlet weak var pPrice: UILabel!
    
}

class ToolsCell: UICollectionViewCell {
    @IBOutlet weak var toolsText: UILabel!
    @IBOutlet weak var toolsImg: UIImageView!
    
}
