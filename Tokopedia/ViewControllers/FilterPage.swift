//
//  FilterPage.swift
//  Tokopedia
//
//  Created by Vanilla Latte on 09/08/18.
//

import UIKit
import RangeSeekSlider
class FilterPage: UIViewController {

    //TOOLBOX
    @IBOutlet weak var minPriceChanged: UILabel!
    
    @IBOutlet weak var maxPriceChanged: UILabel!
    
    @IBOutlet weak var wholeSaleState: UISwitch!
    
    @IBOutlet weak var shopType: UICollectionView!
    
    @IBOutlet weak var priceSlider: RangeSeekSlider!
    
    // VARIABLE
    var backProtocol : fromSearchToPage?
    var params = filterModel()
    var shopTypeList = [String]()
    
    // FUNCTION
    @IBAction func saleState(_ sender: UISwitch) {
        params.wholesale = sender.isOn
    }
    
    @IBAction func shopTypeNext(_ sender: UIButton) {
        let VC: ShopFilterPage = ShopFilterPage(nibName: "ShopFilterPage", bundle: nil)
        VC.delegate = self
        VC.params = params
        self.present(VC, animated: true, completion: nil)
//        self.navigationController?.pushViewController(VC, animated: true)
    }
    @IBAction func doneApply(_ sender: UIButton) {
        backProtocol?.backToSearch(params: params)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func resetFilter(_ sender: UIButton) {
        params.goldType = "2"
        params.officialType = true
        params.pmax = "10000000"
        params.pmin = "100"
        params.wholesale = true
        wholeSaleState.isOn = true
        minPriceChanged.text = "Rp 100"
        maxPriceChanged.text = "Rp 10000000"
        priceSlider.maxValue = 10000000
        priceSlider.minValue = 100
        priceSlider.selectedMaxValue = 10000000
        priceSlider.selectedMinValue = 100
        priceSlider.maxValue = 10000000
        priceSlider.minValue = 100
        priceSlider.selectedMaxValue = 10000000
        priceSlider.selectedMinValue = 100
        shopTypeList.removeAll()
        setShopType()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCloseButton()
        self.setShopType()
        self.setFilter()
        // Do any additional setup after loading the view.
    }
    
    func setFilter(){
        wholeSaleState.isOn = params.wholesale!
        priceSlider.delegate = self
        if let min = NumberFormatter().number(from: params.pmin!) {
            priceSlider.selectedMinValue = CGFloat(min)
        }
        if let max = NumberFormatter().number(from: params.pmax!){
            priceSlider.selectedMaxValue = CGFloat(max)
        }
        let format = NumberFormatter()
        format.numberStyle = .currency
        format.locale = Locale(identifier: "id_ID")
        minPriceChanged.text = format.string(from: Int(priceSlider.selectedMinValue) as NSNumber)
        maxPriceChanged.text = format.string(from: Int(priceSlider.selectedMaxValue) as NSNumber)
        priceSlider.numberFormatter.numberStyle = .currency
        priceSlider.numberFormatter.locale = Locale(identifier: "id_ID")
        priceSlider.minLabelFont = UIFont(name: "ChalkboardSE-Regular", size: 15.0)!
        priceSlider.maxLabelFont = UIFont(name: "ChalkboardSE-Regular", size: 15.0)!
    }
    
    func setShopType(){
        if params.goldType != "0"{
            shopTypeList.append("Gold Merchant")
        }
        if params.officialType! {
            shopTypeList.append("Official Store")
        }
        shopType.reloadData()
    }
    
    func setCloseButton(){
        let img = UIImage(named: "close")?.withRenderingMode(.alwaysOriginal)
        let backButton : UIButton = UIButton(type: .custom)
        backButton.addTarget(self, action: #selector(self.dismissPage(sender:)), for: .touchUpInside)
        backButton.setImage(img, for: .normal)
        backButton.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
        let customBar: UIBarButtonItem = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = customBar
    }
    @objc func dismissPage(sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }

    @objc func removeShopType(sender: UIButton){
        let name: String = shopTypeList[sender.tag]
        shopTypeList = shopTypeList.filter { $0 != name }
        if name == "Gold Merchant" {
            params.goldType = "0"
        }
        else if name == "Official Store" {
            params.officialType = false
        }
        shopType.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FilterPage : RangeSeekSliderDelegate {
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        params.pmin = "\(Int(minValue))"
        params.pmax = "\(Int(maxValue))"
        minPriceChanged.text = "Rp. \(Int(minValue))"
        maxPriceChanged.text = "Rp. \(Int(maxValue))"
    }
}

extension FilterPage : shopTypeProtocol {
    func applyDone(params: filterModel) {
        self.params = params
        shopTypeList.removeAll()
        setShopType()
    }    
}

extension FilterPage : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shopTypeList.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shopTypeCell", for: indexPath) as! shopTypeCell
        cell.txtType.text = shopTypeList[indexPath.row]
        cell.closeBtn.tag = indexPath.row
        cell.closeBtn.addTarget(self, action: #selector(self.removeShopType(sender:)), for: .touchUpInside)
        return cell
    }
    
    
}

class shopTypeCell : UICollectionViewCell {
    @IBOutlet weak var txtType: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    
}
