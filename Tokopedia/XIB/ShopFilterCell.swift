//
//  ShopFilterCell.swift
//  Tokopedia
//
//  Created by Vanilla Latte on 09/08/18.
//

import UIKit
import UICheckbox_Swift
class ShopFilterCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet weak var textType: UILabel!
    @IBOutlet weak var checkType: UICheckbox!
    
}
