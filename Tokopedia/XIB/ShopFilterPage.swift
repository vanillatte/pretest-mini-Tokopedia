//
//  ShopFilterPage.swift
//  Tokopedia
//
//  Created by Vanilla Latte on 09/08/18.
//

import UIKit
class ShopFilterPage: UIViewController {

    @IBOutlet weak var shopTypeTable: UITableView!
    
    
    @IBAction func Reset(_ sender: UIButton) {
        params.goldType = "0"
        params.officialType = false
        typeCheck = [false, false]
        shopTypeTable.reloadData()
    }
    @IBAction func closeBtn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func applyShop(_ sender: UIButton) {
        if typeCheck[0]{
            params.goldType = "2"
        }
        else {
            params.goldType = "0"
        }
        if typeCheck[1]{
            params.officialType = true
        }
        else {
            params.officialType = false
        }
        delegate?.applyDone(params: params)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var applyDone: UIButton!
    var typeList = ["Gold Merchant", "Official Store"]
    var typeCheck = [false, false]
    var params = filterModel()
    var delegate: shopTypeProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
        if params.goldType == "2" {
            typeCheck[0] = true
        }
        if params.officialType! {
            typeCheck[1] = true
        }
        shopTypeTable.separatorStyle = .none
        let shopCell = UINib.init(nibName: "ShopFilterCell", bundle: nil)
        shopTypeTable.register(shopCell, forCellReuseIdentifier: "shopFilterCell")
        shopTypeTable.reloadData()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        shopTypeTable.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        params = filterModel()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkParams(sender: UIButton){
        if sender.tag == 0 {
            if params.goldType == "2" {
                typeCheck[0] = false
            }
            else {
                typeCheck[0] = true
            }
        }
        else if sender.tag == 1 {
            if params.officialType! {
                typeCheck[1] = false
            }
            else {
                typeCheck[1] = true
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ShopFilterPage : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return typeList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "shopFilterCell", for: indexPath) as! ShopFilterCell
        cell.textType.text = typeList[indexPath.row]
        cell.checkType.tag = indexPath.row
        cell.checkType.addTarget(self, action: #selector(self.checkParams(sender:)), for: .touchUpInside)
        cell.checkType.isSelected = typeCheck[indexPath.row]
        
        return cell
    }
    
}
